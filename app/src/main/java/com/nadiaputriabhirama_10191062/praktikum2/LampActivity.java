package com.nadiaputriabhirama_10191062.praktikum2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class LampActivity extends AppCompatActivity {

    Boolean turnOn = false;
    ImageView imageview;
    Button onOffButton, backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lamp);

        imageview = findViewById(R.id.imageView);
        onOffButton = findViewById(R.id.on_off_btn);
        backBtn = findViewById(R.id.back_btn);

        onOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!turnOn){
                    imageview.setImageResource(R.drawable.trans_on);
                    turnOn=true;
                }else {
                    imageview.setImageResource(R.drawable.trans_off);
                    turnOn=false;
                }
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}